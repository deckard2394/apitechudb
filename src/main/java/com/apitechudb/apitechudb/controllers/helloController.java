package com.apitechudb.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class helloController {

    @RequestMapping("/")
    public String index(){
        return "Hola mundo";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "techU") String name){
        return String.format("Holaa %s", name);
    }
}
