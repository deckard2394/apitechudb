package com.apitechudb.apitechudb.controllers;

import com.apitechudb.apitechudb.models.PurchaseModel;
import com.apitechudb.apitechudb.models.UserModel;
import com.apitechudb.apitechudb.services.PurchaseService;
import com.apitechudb.apitechudb.services.PurchaseServiceResponse;
import com.apitechudb.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v3")
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases en Controller");

        return new ResponseEntity<>(
                this.purchaseService.findPurchase(),
                HttpStatus.OK
        );

    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase en Controller");


        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
               this.purchaseService.addPurchase(purchase).getPurchase(),
               this.purchaseService.addPurchase(purchase).getResponseHttpStatusCode()
        );
    }
}
