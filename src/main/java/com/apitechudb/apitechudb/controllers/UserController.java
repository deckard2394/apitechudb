package com.apitechudb.apitechudb.controllers;

import com.apitechudb.apitechudb.models.ProductModel;
import com.apitechudb.apitechudb.models.UserModel;
import com.apitechudb.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user a crear es " + user.getId());
        System.out.println("El nombre del user a crear es " + user.getName());
        System.out.println("La edad del user a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name= "ageFilter", required = false, defaultValue = "0") int ageFilter){
        System.out.println("getUsers");
        System.out.println("ageFilter: " + ageFilter);

        return new ResponseEntity<>(
                this.userService.findAll(ageFilter),
                HttpStatus.OK
        );

    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del user a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "user "+id+" no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("la id del user a actualizar en parámetro es " + id);
        System.out.println("la id del user a actualizar es " + user.getId());
        System.out.println("el nombre del user a actualizar es " + user.getName());
        System.out.println("la edad del user a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del user a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "USER borrado" : "USER "+id+" no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
