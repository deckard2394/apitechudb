package com.apitechudb.apitechudb.controllers;

import com.apitechudb.apitechudb.models.ProductModel;
import com.apitechudb.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// Servicio
// Repositorio ( abstrae la capa de persistencia )
// Autowired:
// Inyector de dependencias:

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                 this.productService.findAll(),
                 HttpStatus.OK
        );

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto a crear es " + product.getId());
        System.out.println("La descripcion del producto a crear es " + product.getDesc());
        System.out.println("El precio del producto a crear es " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
          result.isPresent() ? result.get() : "producto "+id+" no encontrado",
          result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("la id del producto a actualizar en parámetro es " + id);
        System.out.println("la id del producto a actualizar es " + product.getId());
        System.out.println("la descricpion del producto a actualizar es " + product.getDesc());
        System.out.println("el precio del producto a actualizar es " + product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
               deleteProduct ? "producto borrado" : "producto "+id+" no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
