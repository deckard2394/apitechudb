package com.apitechudb.apitechudb.repositories;

import com.apitechudb.apitechudb.ApitechudbApplication;
import com.apitechudb.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findPurchase(){
        System.out.println("findPurchase en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("findPurchase en PurchaseRepository");
        ApitechudbApplication.purchaseModels.add(purchase);
        return purchase;
    }

    public Optional<PurchaseModel> findPurchaseById(String id){
        System.out.println("findPurchaseById en PurchaseRepository");
        Optional<PurchaseModel> purchase = Optional.empty();

        for(PurchaseModel purchaseInList : ApitechudbApplication.purchaseModels){
            if(purchaseInList.getUserId().equals(id)){
                System.out.println("Purchase encontrada con ID: " + id);
                purchase = Optional.of(purchaseInList);
            }
        }
        return purchase;
    }


}
