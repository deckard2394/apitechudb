package com.apitechudb.apitechudb.repositories;

import com.apitechudb.apitechudb.ApitechudbApplication;
import com.apitechudb.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductRepository");

        return ApitechudbApplication.productModels;
    }

    public ProductModel save(ProductModel product){
        System.out.println("Save en ProductRepository");
        ApitechudbApplication.productModels.add(product);
        return product;
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductRepository");

        Optional<ProductModel> result = Optional.empty();

        for(ProductModel productInList : ApitechudbApplication.productModels){
            if(productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                System.out.println("Producto con la id " + id + " encontrado");
                result = Optional.of(productInList);
            }
        }

        return result;
    }

    public ProductModel update(ProductModel product){
        System.out.println("update en ProductRepository");

        Optional<ProductModel> prodcuctToUpdate = this.findById(product.getId());

        if(prodcuctToUpdate.isPresent()){
            System.out.println("producto para actualizar encontrado");

            ProductModel productFromList = prodcuctToUpdate.get();

            productFromList.setDesc(product.getDesc());
            productFromList.setPrice(product.getPrice());

        }
        return product;
    }

    public void delete(ProductModel product){
        System.out.println("delete en ProductRepository");
        System.out.println("Borrando producto");
        ApitechudbApplication.productModels.remove(product);
    }
}
