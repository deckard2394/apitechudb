package com.apitechudb.apitechudb;

import com.apitechudb.apitechudb.models.ProductModel;
import com.apitechudb.apitechudb.models.PurchaseModel;
import com.apitechudb.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getTestDataUser();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestDataPurchase();

	}

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> productModels = new ArrayList<>();
		productModels.add(
				new ProductModel(
						"1"
						,"producto 1"
						,10
				)
		);
		productModels.add(
				new ProductModel(
						"2"
						,"producto 2"
						,20
				)
		);
		productModels.add(
				new ProductModel(
						"3"
						,"producto 3"
						,30
				)
		);
		productModels.add(
				new ProductModel(
						"4"
						,"producto 4"
						,40
				)
		);
		return productModels;
	}

	private static ArrayList<UserModel> getTestDataUser(){
		ArrayList<UserModel> userModels = new ArrayList<>();
		userModels.add(
				new UserModel(
						"1"
						,"Nombre 1"
						,20
				)
		);
		userModels.add(
				new UserModel(
						"2"
						,"Nombre 2"
						,20
				)
		);		userModels.add(
				new UserModel(
						"3"
						,"Nombre 3"
						,30
				)
		);
		return userModels;
	}

	private static ArrayList<PurchaseModel> getTestDataPurchase(){
		return new ArrayList<>();
	}
}
