package com.apitechudb.apitechudb.models;

public class UserModel {

    private String id;
    private String name;
    private int age;

// los CONSTRUCTORES se llaman igual que su clase y nunca devuelven nada
// constructor sin parámetros:
    public UserModel() {
    }
    // constructor con parámetros:
    public UserModel(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
