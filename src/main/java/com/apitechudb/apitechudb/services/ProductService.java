package com.apitechudb.apitechudb.services;

import com.apitechudb.apitechudb.ApitechudbApplication;
import com.apitechudb.apitechudb.models.ProductModel;
import com.apitechudb.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll in ProductService");

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add en productService");

        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel productModel){
        System.out.println("Update en productService");

        return this.productRepository.update(productModel);
    }

    public boolean delete(String id){
        System.out.println("delete en ProductService");

        boolean result = false;

        Optional<ProductModel> productToDelete = this.findById(id);

        if(productToDelete.isPresent()){
            result = true;
            this.productRepository.delete(productToDelete.get());
        }
        return result;

    }

}
