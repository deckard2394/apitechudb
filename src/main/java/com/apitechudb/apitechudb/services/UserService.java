package com.apitechudb.apitechudb.services;

import com.apitechudb.apitechudb.ApitechudbApplication;
import com.apitechudb.apitechudb.models.UserModel;
import com.apitechudb.apitechudb.repositories.ProductRepository;
import com.apitechudb.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

// para usar inyector de dependencias    autowired
    @Autowired
    UserRepository userRepository;


    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public List<UserModel> findAll(int ageFilter){
        System.out.println("findAll in UserService");

        if (ageFilter <= 0){
            return this.userRepository.findAll();
        }
        System.out.println("agefilter es "+ ageFilter );

        ArrayList<UserModel> result = new ArrayList<>();

        for(UserModel userInList : ApitechudbApplication.userModels){
            if(userInList.getAge() == ageFilter){
                System.out.println("Usuario " + userInList.getName() + " tiene misma edad que filtro.");
                result.add(userInList);
            }
        }
        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel){
        System.out.println("Update en userService");

        return this.userRepository.update(userModel);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if(userToDelete.isPresent()){
            result = true;
            this.userRepository.delete(userToDelete.get());
        }
        return result;

    }


}
