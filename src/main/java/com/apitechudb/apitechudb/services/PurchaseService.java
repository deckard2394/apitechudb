package com.apitechudb.apitechudb.services;

import com.apitechudb.apitechudb.ApitechudbApplication;
import com.apitechudb.apitechudb.models.PurchaseModel;
import com.apitechudb.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    public List<PurchaseModel> findPurchase() {
        System.out.println("findPurchase en ShopService");
        return this.purchaseRepository.findPurchase();
    }

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase){
        System.out.println("addPurchase en Repository");

        PurchaseServiceResponse result = new PurchaseServiceResponse();
        result.setPurchase(purchase);

        if(this.userService.findById(purchase.getUserId()).isPresent()==false){
            System.out.println("El usuario de la compra no se ha encontrado");
            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return result;
        }

        if(this.getById(purchase.getId()).isPresent()==true){
            System.out.println("Ya hay una compra con esa ID");

            result.setMsg("Ya hay una compra con esa ID");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        float amount=0;
        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()){
            if (this.productService.findById(purchaseItem.getKey()).isPresent()==false){
                System.out.println("EL producto con la id "+purchaseItem.getKey()+ "no se encuentra");

                result.setMsg("EL producto con la id "+purchaseItem.getKey()+ "no se encuentra");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            }else{
                amount += this.productService.findById(purchaseItem.getKey()).get().getPrice()
                        * purchaseItem.getValue();
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);
        return result;
    }

    public Optional<PurchaseModel> getById(String id){
        System.out.println("getById en PurchaseService");
        System.out.println("la id es: " + id);
        return this.purchaseRepository.findPurchaseById(id);
    }
}
